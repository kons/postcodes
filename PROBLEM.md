# Problem


* We want you to create an API in Java that allows mobile clients to retrieve and
add suburb and postcode combinations. You do not have to write the mobile
app!

* We want you to implement:

  * An API that allows mobile clients to retrieve the suburb information by postcode.

  * An API that allows mobile clients to retrieve a postcode given a suburb name


* A secured API to add new suburb and postcode combinations (you'll have
to work out how this should work)


* Some form of persistence


Please do NOT spend more than 6 hours on this exercise. We don’t intend to
measure how fast you can code, rather the way you go about it.


# Design Considerations


* Best Practice

  * Develop with current best practice

  * Design patterns, software architecture, secure coding

  * Libraries, industry standards


* Reusability

  * Is there anything that can be reused in future projects?


* Support knowledge sharing

  * Documentation

  * Comments

  * Anything can help with knowledge sharing with the technical team


* Development Process and Quality Control

  * Testing



# Submission

* Complete Implementation Project files which allow AusPost to build your
solution without any modification, and deploy it on an AWS EC2 (Amazon
Linux) instance.

* If required, you can also provide instructions on the build and deploy process.

* You can also provide a short design document if you want to, but this is NOT a
mandatory item.

* Build script, testing report, any documentation you want to show us.

* All References

* 3rd party Source code and libraries

* Open source projects

* Other references
