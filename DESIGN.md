Initial Considerations
------------------

- [ ] Setup an API project
    - [ ] Evaluate modern frameworks, [spring data rest + spring security](https://github.com/spring-projects/spring-data-examples/tree/master/rest/security)?
    - [ ] Ensure the framework picked is testable
    - [ ] Ensure can build a war, that is deployable to EC2. 
        - [ ] Consider [Gradle AWS plugin](https://github.com/classmethod/gradle-aws-plugin) to orchestrate EC2
    - [ ] Some persistence framework.  Jooq?
- [ ] Quality metrics in the build.
- [ ] What considerations around mobile clients and security do we need to consider?  HSTS, CORS, etc
    - [ ] Navigable API via HAL & Hateoas 
    
        
#### API considerations
- [ ] Logging of requests, plus where do the app logs go?
- [ ] Concurrency: multiple hosts writing to same db table
- [ ] Authentication of users, session management
- [ ] Caching. 
- [ ] Config
- [ ] Automated API Documentation

#### Cloud considerations
http://12factor.net

- [ ] Failover
- [ ] Service Discovery
- [ ] No sticky sessions
- [ ] Use cloudformation to bring together the 'stack' in AWS

**These are nice to have but doubt I'll have time**

### Problem Specific Considerations
* The postcodes have two 'read' endpoints.  suburbs by postcode.  postcodes by suburb
   * Each input can return more than one answer. https://en.wikipedia.org/wiki/Postcodes_in_Australia
* These could be GET requests with path or query params for the inputs.
   * Responses can be cached aggressively since this data is unlikely to change (only new entries appended to)
* No requirement to secure the read endpoints
   * If there was, inputs could be POST requests to hide input in transit
* Auth of the secured API to submit suburb and postcode combinations
* Posting the same entity multiple times
* Use YAML instead of .properties for configuration
