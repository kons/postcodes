package org.outrospective.post.service;

import org.outrospective.post.domain.Suburb;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "suburbs", path = "suburbs")
public interface SuburbRepository extends PagingAndSortingRepository<Suburb, Long> {

    List<Suburb> findByNameAllIgnoringCase(@Param("name") String name);
    List<Suburb> findByPostcode(@Param("postcode") String postcode);

}
