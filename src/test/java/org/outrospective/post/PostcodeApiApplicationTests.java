package org.outrospective.post;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.outrospective.post.domain.Suburb;
import org.outrospective.post.service.SuburbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles( "scratch")
// Separate profile for web tests to avoid clashing databases
public class PostcodeApiApplicationTests {

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private SuburbRepository repository;

	private MockMvc mvc;

	private Iterable<Suburb> persistedSuburbs;

	@Before
	public void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Before
	public void persistTestSuburbs() {
		persistedSuburbs = repository.save(Arrays.asList(
				new Suburb("Melbourne", "3000"),
				new Suburb("Melbourne", "8000")
		));
	}

	@After
	public void deleteTestSuburbs() {
		repository.delete(persistedSuburbs);
	}

	@Test
	public void testHome() throws Exception {
		mvc.perform(get("/api")).andExpect(status().isOk())
				.andExpect(content().string(containsString("suburbs")));
	}

	@Test
	public void findByNameAndCountry() throws Exception {
		mvc.perform(
			get("/api/suburbs/search/findByNameAllIgnoringCase?name=Melbourne"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("_embedded.suburbs.*.postcode", containsInAnyOrder("3000", "8000")))
			.andExpect(jsonPath("_embedded.suburbs.*.name", contains("Melbourne", "Melbourne")));
	}

	@Test
	public void findByPostcode() throws Exception {
		mvc.perform(
			get("/api/suburbs/search/findByPostcode?postcode=3000"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("_embedded.suburbs", hasSize(1)))
			.andExpect(jsonPath("_embedded.suburbs[0].postcode", equalTo("3000")));
	}

	@Test
	public void postNewSuburb() throws Exception {
		mvc.perform(
				post("/api/suburbs")
				.contentType(MediaType.APPLICATION_JSON)
				.content(" { \"name\": \"Sydney\", \"postcode\":\"2000\" } "))
				.andExpect(status().isCreated());

		assertThat(repository.findByPostcode("2000")).extracting("name").contains("Sydney");
	}

}
