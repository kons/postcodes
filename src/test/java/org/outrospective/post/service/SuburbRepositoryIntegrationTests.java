package org.outrospective.post.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.outrospective.post.domain.Suburb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SuburbRepositoryIntegrationTests {

    @Autowired
    private SuburbRepository repository;

    private static Iterable<Suburb> testSuburbs;

    @Before
    public void createSuburbs() {
        List<Suburb> randomSuburbs = IntStream.range(0, 10)
                .mapToObj(idx -> new Suburb(
                        randomAlphabetic(8),
                        randomNumeric(4)))
                .collect(toList());
        testSuburbs = repository.save(randomSuburbs);
    }

    @After
    public void deleteSuburbs() {
        repository.delete(testSuburbs);
    }

    @Test
    public void findsFirstPageOfSuburbs() {
        Page<Suburb> suburbs = repository.findAll(new PageRequest(0, 10));
        assertThat(suburbs.getTotalElements()).isEqualTo(10L);
    }

    @Test
    public void findByName() {
        String suburbName = testSuburbs.iterator().next().getName();
        List<Suburb> suburbs = repository.findByNameAllIgnoringCase(suburbName.toUpperCase());
        assertThat(suburbs).isNotEmpty();
        assertThat(suburbs).extracting(Suburb::getName).containsOnly(suburbName);
    }

    @Test
    public void findByPostcode() {
         String testPostcode = testSuburbs.iterator().next().getPostcode();
         List<Suburb> suburbs = repository.findByPostcode(testPostcode);

         assertThat(suburbs).hasSize(1);
         assertThat(suburbs).extracting(Suburb::getPostcode).containsOnly(testPostcode);
    }
}
