# Postcodes problem

See [PROBLEM](PROBLEM.md) for summary problem.

See [DESIGN](DESIGN.md) for some thoughts on design & other deliverable's for this exercise.

## The app
Runs on Java 8.  Built with Gradle
Uses Spring Boot with Spring Data Rest

## Running app locally
`./gradlew bootRun`

[http://localhost:8080/api](http://localhost:8080/api)

Spring Boot Actuator is used to provide metrics on the endpoints.  See http://localhost:8080/info

### Persistence
spring-data-rest-jpa writes to a in memory h2 db by default.

### Authentication
Basic authentication is used when creating a new suburb.   The username is 'user'. The password is 'password'

`curl -d "{ \"name\": \"Melbourne\", \"postcode\":\"3000\" }" user:password@localhost:8080/api/suburbs -H "Content-Type: application/json"`

# Code Quality 
Findbugs and Checkstyle are wired into the build.  Builds will fail for High level issues. 

# References

- [Spring Boot Documentation]()
- [Spring Boot Samples](https://github.com/spring-projects/spring-boot/tree/1.5.x/spring-boot-samples/spring-boot-sample-data-rest)
- [Spring Security Starter](http://docs.spring.io/spring-security/site/docs/current/guides/html5//helloworld-boot.html)
- [Building a secure REST API with Spring Data REST and Java 8](https://jaxenter.com/rest-api-spring-java-8-112289.html)
